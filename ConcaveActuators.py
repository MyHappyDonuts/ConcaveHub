# User imports
import ConcaveLogging

# Constants
ID_DEHUMIDIFIER = "Dehumidifier"
ID_BOOKSHLEF_LIGHTS = "Bookshelf_lights"

# Global variables
actuator_dehumidifier = None
actuator_bookshelf_lights = None

def Init():
    global actuator_dehumidifier
    global actuator_bookshelf_lights

    actuator_dehumidifier = Actuator_type(ID_DEHUMIDIFIER)
    actuator_bookshelf_lights = Actuator_type(ID_BOOKSHLEF_LIGHTS)

class Actuator_type:
    def __init__(self, id):
        self.id = id
        self.state = False