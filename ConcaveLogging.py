# Libary imports
from datetime import datetime

# Constants
LOG_PATH = "Log_files/logfile.log"
WARNING_PATH = "Log_files/warnings.log"

LOG_ENABLED = False

def log (id, msg):
    now = datetime.now()
    # YY/mm/dd H:M:S
    timestamp = now.strftime("%Y/%m/%d-%H:%M:%S - ")

    log_line = timestamp + id + ": " + msg

    # Print log line
    print(log_line)

    # Write log line to log file
    if (LOG_ENABLED):
        with open(LOG_PATH, 'a', encoding='UTF8', newline='') as f:
            f.write(log_line + "\n")

def warning (id, msg):
    now = datetime.now()
    # YY/mm/dd H:M:S
    timestamp = now.strftime("%Y/%m/%d-%H:%M:%S - ")

    log_line = timestamp + id + ": *WARNING* " + msg

    # Print log line
    print(log_line)
    
    # Write log line to standard log file
    if (LOG_ENABLED):
        with open(LOG_PATH, 'a', encoding='UTF8', newline='') as f:
            f.write(log_line + "\n")

    # Write log line to warning log file
    with open(WARNING_PATH, 'a', encoding='UTF8', newline='') as f:
        f.write(log_line + "\n")
