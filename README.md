# Concave Hub



## Introduction

This paragraph is the introduction. It briefly describes the project

## Operating principle

Thi paragraph details how the application works.

## Prerequisites

### Hardware prerequisites

- **Embedded Linux device**: In my case, I used a Raspberry Pi 3b, but any Linux capable system should work. It should be even possible to make it work on a Windows system
- **Wi-Fi or any LAN interface**: Since Concave Hub can work through MQTT, LAN interface is recommended. Wi-Fi interface should be ideal 
- **Zigbee gateway** (optional): If Zigbee devices are considered, a Zigbee gateway will be necessary. In my case I used a SONOFF USB Zigbee dongle. 
- **BLE interface** (TBD)

### Software prerequisites

- **Linux OS**: If running on an embedded Linux system, a Linux OS will be necessary. In this case I simply used Raspberry Pi OS Lite.
- **Python and pip**: At this moment, having Python installed is necessary to run the application. Required packages are installed through pip. In this case I used [Python](https://www.python.org/downloads/) 3.11.0. 
- **MQTT broker**: If MQTT protocol is used, a broker needs to be installed beforehand. In this case I used [Mosquitto](https://mosquitto.org/)
- **node and npm** (optional): At this moment, [node](https://nodejs.org/en/) is only required for the web application used by zigbee2mqtt. Therefore, it is not mandatory. However, in the future the user parameters will be modified through a HMI web app, so it may eventually be required. It may be tricky to install depending on the hardware used
- **cron/crontab** (optional): Scheduling tool usually included in Linux OS used to launch the startup script on system startup and watchdog scripts to check if everything is running properly through time. 
- **zigbee2mqtt** (optional): If Zigbee devices are involved, [zigbee2mqtt](https://www.zigbee2mqtt.io/) tool is required. It acts as an interface between Zigbee devices and an MQTT broker.

## Installation

How to install Concave Hub app and get it to work. How to set up the environment to add new features

## Development environment

Install Visucal Studio Code
Install Plugins:
- Python
- Markdown
- CSV viewer

Virtual environment:

Create a virtual environment (only first time):
```bash
python -m venv venv
```

Activate it:

```bash
# In PowerShell
venv\Scripts\Activate.ps1
```

Create a file called `requirements.txt`, containing every package needed by the application. For example:


```bash
paho-mqtt==1.6.1
schedule==1.1.0
```

To automatically create the requirements.txt file with all the pacakges included:

```bash
pip freeze > `requirements.txt`
```

Install required packages from requirements.txt:

```bash
pip install -r `requirements.txt`

```

***

## Modules

Here are the modules used by the application.

Module diagram

### Main

This is the main module. Here are its contents:
- Modules initialization
- Tasks definition
- Tasks scheduler
- Main loop

### Mqtt

This module implements an MQTT client:
- Subscription to Concave devices MQTT topics
- JSON messages parsing 

### Parameters

JSON parameters

### Processing

This module processed received data from Concave devices. It has two main jobs:
- Every 15 minutes, a new line contaning a timestamp and sensor data from all devices is created in memory
- Every 60 minutes, created lines are atored into a CSV file

The purpose of this is to reduce write acces to the SD card. If for some reason the application crashes, only the data from the last hour will be lost in the worst case.

### SensorData

Sensor data structures

### Actuators

Actuator data structures

### Automation

Automation module. Defines the behaviour of the actuators based on the parameters and sensor data

### Logging

Allows logfiles to record the activity of the application. Only for debugging purposes

## Scripts

### Startup script

### Watchdog script

***

## To-do list

- HMI (web app?)
- Bookshelf lights start and stop times determined from the parameter instead being hard-coded
- Openweather data
- Creating an executable
- Visualize application (from CSV?)
- BLE support to replace WiFi

***

## License
