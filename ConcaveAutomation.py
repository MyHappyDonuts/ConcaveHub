# Library imports
from datetime import datetime
from lzma import MODE_NORMAL

# User imports
import ConcaveSensorData
import ConcaveParameters
import ConcaveLogging
import ConcaveActuators

# Constants
LOG_ID = "AUTO"

# Dehumidifier state
DEHUMI_OFF = "OFF"
DEHUMI_ON = "ON"

# Operating modes
MODE_NORMAL = 0
MODE_DEGRADED = 1
MODE_HOLIDAYS = 2
MODE_MANUAL = 3

# Schedule mode
SCHEDULE_WORK = 0
SCHEDULE_DAY_OFF = 1

# Global variables
operating_mode = MODE_NORMAL
schedule_mode = SCHEDULE_WORK

window_start_dehumidifier = None
window_stop_dehumidifier = None
window_start_bookshelf = None
window_stop_bookshelf = None

def Init():
    SetActivityWindows()

# Handles the behaviour of the dehumidifier
def DehumidifierSchedule():
    if (DehumidifierTimeWindow()):
        ConcaveLogging.log(LOG_ID, "We are inside the time window")
        if (ConcaveSensorData.SensorData_Type.active_sensors > 0):
            DehumidifierHumidityTresholds()
        else:
            ConcaveLogging.log(LOG_ID, "Dehumidifier " + DEHUMI_OFF)
    elif (ConcaveSensorData.dehumidifier_on):
        ConcaveLogging.log(LOG_ID, "Dehumidifier " + DEHUMI_OFF)
    else:
        ConcaveLogging.log(LOG_ID, "We are outside the time window")

def GetBookshelfStart():
    bookshelf_start_str = str(window_start_bookshelf.hour) + ":" + str(window_start_bookshelf.minute)
    return bookshelf_start_str

def GetBookshelfStop():
    bookshelf_stop_str = str(window_stop_bookshelf.hour) + ":" + str(window_stop_bookshelf.minute)
    return bookshelf_stop_str

def SetActivityWindows():
    global schedule_mode
    global window_start_dehumidifier
    global window_stop_dehumidifier
    global window_start_bookshelf
    global window_stop_bookshelf
    now = datetime.now()

    # Weekday is Saturday or Sunday
    if (now.weekday() >= 5):
        schedule_mode = SCHEDULE_DAY_OFF
        window_start_dehumidifier = datetime.now().replace(hour = ConcaveParameters.dehumidifier.day_off_time.start_hour, minute = ConcaveParameters.dehumidifier.day_off_time.start_minute, second = 0, microsecond = 0)
        window_stop_dehumidifier = datetime.now().replace(hour = ConcaveParameters.dehumidifier.day_off_time.stop_hour, minute = ConcaveParameters.dehumidifier.day_off_time.start_minute, second = 0, microsecond = 0)
    
        window_start_bookshelf = datetime.now().replace(hour = ConcaveParameters.bookshelf_lights.day_off_time.start_hour, minute = ConcaveParameters.dehumidifier.day_off_time.start_minute, second = 0, microsecond = 0)
        window_stop_bookshelf = datetime.now().replace(hour = ConcaveParameters.bookshelf_lights.day_off_time.stop_hour, minute = ConcaveParameters.dehumidifier.day_off_time.start_minute, second = 0, microsecond = 0)
    else:
        schedule_mode = SCHEDULE_WORK
        window_start_dehumidifier = datetime.now().replace(hour = ConcaveParameters.dehumidifier.work_time.start_hour, minute = ConcaveParameters.dehumidifier.work_time.start_minute, second = 0, microsecond = 0)
        window_stop_dehumidifier = datetime.now().replace(hour = ConcaveParameters.dehumidifier.work_time.stop_hour, minute = ConcaveParameters.dehumidifier.work_time.start_minute, second = 0, microsecond = 0)

        window_start_bookshelf = datetime.now().replace(hour = ConcaveParameters.bookshelf_lights.work_time.start_hour, minute = ConcaveParameters.bookshelf_lights.work_time.start_minute, second = 0, microsecond = 0)
        window_stop_bookshelf = datetime.now().replace(hour = ConcaveParameters.bookshelf_lights.work_time.stop_hour, minute = ConcaveParameters.bookshelf_lights.work_time.stop_minute, second = 0, microsecond = 0)


# Local functions

# Activates the dehumidifier if the humidity is outside the thresholds
def DehumidifierHumidityTresholds():
    humidity_avg = None

    if (ConcaveSensorData.sensor_desk.data_new):
        if (ConcaveSensorData.sensor_living.data_new):
            humidity_avg = (ConcaveSensorData.sensor_desk.humidity + ConcaveSensorData.sensor_living.humidity) / 2
        else:
            humidity_avg = ConcaveSensorData.sensor_desk.humidity
    elif (ConcaveSensorData.sensor_living.data_new):
        humidity_avg = ConcaveSensorData.sensor_living.humidity

    if ((humidity_avg >= ConcaveParameters.dehumidifier.humidity_max) and (ConcaveActuators.actuator_dehumidifier.state == False)):
        ConcaveActuators.actuator_dehumidifier.state = True
        ConcaveLogging.log(LOG_ID, "Dehumidifier " + DEHUMI_ON)
        # Send MQTT message to turn ON the actuator
    elif ((humidity_avg <= ConcaveParameters.dehumidifier.humidity_min) and (ConcaveActuators.actuator_dehumidifier.state == True)):
        ConcaveActuators.actuator_dehumidifier.state = False
        ConcaveLogging.log(LOG_ID, "Dehumidifier " + DEHUMI_OFF)
        # Send MQTT message to turn OFF the actuator

# Returns True if we are inside the time window
def DehumidifierTimeWindow():
    bRet = False
    now = datetime.now()

    if ((now >= window_start_dehumidifier) and (now <= window_stop_dehumidifier)): 
        bRet = True
    else:
        bRet = False
    return bRet