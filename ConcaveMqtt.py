# Library imports
import paho.mqtt.client as mqtt
import json
from datetime import datetime

# User imports
import ConcaveLogging
import ConcaveSensorData
import ConcaveActuators

# Constants
LOG_ID = "MQTT"
        
# The callback for when the client receives a CONNACK response from the server.
def on_connect(client, userdata, flags, rc):
    ConcaveLogging.log(LOG_ID, "Connected with result code "+str(rc))

    # Subscribing in on_connect() means that if we lose the connection and
    # reconnect then subscriptions will be renewed.
    client.subscribe("home/#")

# The callback for when a PUBLISH message is received from the server.
def on_message(client, userdata, msg):
    # datetime object containing current date and time
    now = datetime.now()
    
    # dd/mm/YY H:M:S
    dt_string = now.strftime("%Y/%m/%d %H:%M:%S")
    
    # JSON parsing
    msg_json = json.loads(msg.payload)
 
    if(msg.topic == "home/desk/sensor"):
    
        # Line composition
        data = [dt_string, msg_json["humi"], msg_json["temp"]]
        ConcaveLogging.log(LOG_ID, dt_string + " topic: " + msg.topic + " payload: " + str(msg_json))
                
        ConcaveSensorData.sensor_desk.Add_data(msg_json["humi"], msg_json["temp"])
    
    elif(msg.topic == "home/living_room/sensor"):
    
        # Line composition
        data = [dt_string, msg_json["humi"], msg_json["temp"]]
        ConcaveLogging.log(LOG_ID, dt_string + " topic: " + msg.topic + " payload: " + str(msg_json))
        
        ConcaveSensorData.sensor_living.Add_data(msg_json["humi"], msg_json["temp"])
            
    elif(msg.topic == "home/window/sensor"):
    
        # Line composition
        data = [dt_string, msg_json["humi"], msg_json["temp"]]
        print(dt_string + " topic: " + msg.topic + " payload: " + str(msg_json))
        
def Init():
    ConcaveActuators.Init()
    GetActuatorsState()

def GetActuatorsState():
    # Send MQTT requests to get actuators state
    print ("Send MQTT request to get dehumidifier state")
    print ("Send MQTT request to get bookshelf lights state")
    
        