# User imports
import ConcaveLogging

# Constants
LOG_ID = "DATA"

dehumidifier_on = False

# Device ID
ID_DESK_SENSOR = "Desk_sensor"
ID_LIVING_ROOM_SENSOR = "Living_sensor"
ID_UPSTAIRS_SENSOR = "Upstairs_sensor"

# Expiration timer for each sensor (seconds)
EXPIRATION_TIMER = 900

# Global variables
sensor_desk = None
sensor_living = None
sensor_upstairs = None

def Init():
    global sensor_desk
    global sensor_living
    global sensor_upstairs

    sensor_desk = SensorData_Type(ID_DESK_SENSOR, EXPIRATION_TIMER)
    sensor_living = SensorData_Type(ID_LIVING_ROOM_SENSOR, EXPIRATION_TIMER)
    sensor_upstairs = SensorData_Type(ID_UPSTAIRS_SENSOR, EXPIRATION_TIMER)

class SensorData_Type:
    sensors = []
    active_sensors = 0

    def __init__(self, id, expiration_period):
        self.id = id
        self.expiration_period = expiration_period
        self.humidity = None
        self.temperature = None
        self.data_new = False
        self.expiration_counter = 0
        SensorData_Type.sensors.append(self)

    def __str__(self):
        return f"ID: {self.id}, expiration: {self.expiration_period} s, humidity: {self.humidity}"

    def Add_data(self, humidity, temperature):
        self.humidity = humidity
        self.temperature = temperature

        if (self.data_new == False):
            ConcaveLogging.warning(LOG_ID, self.id + ": new data")
            SensorData_Type.active_sensors = SensorData_Type.active_sensors + 1
            self.data_new = True
        self.expiration_counter = self.expiration_period    

    # Method to be called every second
    def Manage_expiration():
        for i in range(len(SensorData_Type.sensors)):
            if (SensorData_Type.sensors[i].expiration_counter > 0):
                SensorData_Type.sensors[i].expiration_counter = SensorData_Type.sensors[i].expiration_counter - 1
            else:
                if (SensorData_Type.sensors[i].data_new == True):
                    ConcaveLogging.warning(LOG_ID, SensorData_Type.sensors[i].id + ": expired")
                    SensorData_Type.sensors[i].data_new = False
                    SensorData_Type.active_sensors = SensorData_Type.active_sensors - 1

                    if (SensorData_Type.active_sensors == 0):
                        ConcaveLogging.warning(LOG_ID, "All sensors expired")


