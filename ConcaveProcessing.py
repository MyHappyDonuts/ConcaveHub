# Library imports
import os
import csv  
from datetime import datetime

# User imports
import ConcaveLogging
import ConcaveSensorData

# Constants
LOG_ID = "PROC"

data_path = "Data_archive/"
header = ['timestamp', 'Desk humidity (%)', 'Living humidity (%)', 'Desk Temp (°C)', 'Living Temp (°C)']

# A line is created and added to the list
def AddDataLine(data_array):
    data_line = []
    sensor_desk_humidity = None
    sensor_living_humidity = None
    sensor_desk_temperature = None
    sensor_living_temperature = None

    # datetime object containing current date and time
    now = datetime.now()
    # YY/mm/dd H:M:S
    timestamp = now.strftime("%Y/%m/%d %H:%M:%S")   

    if (ConcaveSensorData.SensorData_Type.active_sensors > 0):
        data_line.append(timestamp)

        if (ConcaveSensorData.sensor_desk.data_new):
            sensor_desk_humidity = ConcaveSensorData.sensor_desk.humidity
            sensor_desk_temperature = ConcaveSensorData.sensor_desk.temperature

        if (ConcaveSensorData.sensor_living.data_new):
            sensor_living_humidity = ConcaveSensorData.sensor_living.humidity
            sensor_living_temperature = ConcaveSensorData.sensor_living.temperature

        data_line.append(sensor_desk_humidity)
        data_line.append(sensor_living_humidity)
        data_line.append(sensor_desk_temperature)
        data_line.append(sensor_living_temperature)
        data_array.append(data_line)


def Init():
    now = datetime.now()   
    # 2022-41-Oct format
    timestamp_week = now.strftime("%Y-%W-%b")       

    if(os.path.isfile(data_path + timestamp_week + ".csv") == False):
        ConcaveLogging.log(LOG_ID, "Data archive file does not exist. File created")
        with open(data_path + timestamp_week + ".csv", 'a', encoding='UTF8', newline='') as f:
            writer = csv.writer(f)
            # write the header
            writer.writerow(header)

# Lines from the list are added to CSV file
def StoreLines(data_array):
    now = datetime.now()   
    # 2022-41-Oct format
    timestamp_week = now.strftime("%Y-%W-%b")  

    # Check if file has been deleted
    if(os.path.isfile(data_path + timestamp_week + ".csv") == False):
        with open(data_path + timestamp_week + ".csv", 'a', encoding='UTF8', newline='') as f:
            writer = csv.writer(f)
            # write the header
            writer.writerow(header)

    with open(data_path + timestamp_week + ".csv", 'a', encoding='UTF8', newline='') as f:
        writer = csv.writer(f)
        # write data rows
        writer.writerows(data_array)

    data_array.clear() 
    ConcaveLogging.log(LOG_ID, "Data stored to the CVS file")
