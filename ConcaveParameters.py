# Library imports
import json
import os

# Constants
LOG_ID = "PARA"
PARAMETERS_PATH = "Parameters.json"

class ActivityTime_Type:
    def __init__(self, start_hour, start_minute, stop_hour, stop_minute):
        self.start_hour = start_hour
        self.start_minute = start_minute
        self.stop_hour = stop_hour
        self.stop_minute = stop_minute

class Dehumidifier_Type:
    def __init__(self, humidity_min, humidity_max, work_time, day_off_time):
        self.humidity_min = humidity_min
        self.humidity_max = humidity_max
        self.work_time = work_time
        self.day_off_time = day_off_time

class Bookshelf_Type:
    def __init__(self, work_time, day_off_time):
        self.work_time = work_time
        self.day_off_time = day_off_time

dehumidifier = None
bookshelf_lights = None

def Init():
    ReadParameters()

def ReadParameters():
    global dehumidifier
    global bookshelf_lights

    if(os.path.isfile(PARAMETERS_PATH) == False):
        print("Parameters file does not exist!")
                    
    else:
        f = open("Parameters.json")
        parameters = json.load(f)

        DEHUM_START_WORK_HOUR = parameters["dehumidifier"]["work"]["start_hour"]
        DEHUM_START_WORK_MIN = parameters["dehumidifier"]["work"]["start_minute"]
        DEHUM_STOP_WORK_HOUR = parameters["dehumidifier"]["work"]["stop_hour"]
        DEHUM_STOP_WORK_MIN = parameters["dehumidifier"]["work"]["stop_minute"]

        DEHUM_START_DAY_OFF_HOUR = parameters["dehumidifier"]["day_off"]["start_hour"]
        DEHUM_START_DAY_OFF_MIN = parameters["dehumidifier"]["day_off"]["start_minute"]
        DEHUM_STOP_DAY_OFF_HOUR = parameters["dehumidifier"]["day_off"]["stop_hour"]
        DEHUM_STOP_DAY_OFF_MIN = parameters["dehumidifier"]["day_off"]["stop_minute"]

        HUMIDITY_MIN = parameters["dehumidifier"]["humidity_min"]
        HUMIDITY_MAX = parameters["dehumidifier"]["humidity_max"]

        BOOK_START_WORK_HOUR = parameters["bookshelf_lights"]["work"]["start_hour"]
        BOOK_START_WORK_MIN = parameters["bookshelf_lights"]["work"]["start_minute"]
        BOOK_STOP_WORK_HOUR = parameters["bookshelf_lights"]["work"]["stop_hour"]
        BOOK_STOP_WORK_MIN = parameters["bookshelf_lights"]["work"]["stop_minute"]

        BOOK_START_DAY_OFF_HOUR = parameters["bookshelf_lights"]["day_off"]["start_hour"]
        BOOK_START_DAY_OFF_MIN = parameters["bookshelf_lights"]["day_off"]["start_minute"]
        BOOK_STOP_DAY_OFF_HOUR = parameters["bookshelf_lights"]["day_off"]["stop_hour"]
        BOOK_STOP_DAY_OFF_MIN = parameters["bookshelf_lights"]["day_off"]["stop_minute"]

        dehum_work_time = ActivityTime_Type(DEHUM_START_WORK_HOUR, DEHUM_START_WORK_MIN, DEHUM_STOP_WORK_HOUR, DEHUM_STOP_WORK_MIN)
        dehum_day_off_time = ActivityTime_Type(DEHUM_START_DAY_OFF_HOUR, DEHUM_START_DAY_OFF_MIN, DEHUM_STOP_DAY_OFF_HOUR, DEHUM_STOP_DAY_OFF_MIN)
        dehumidifier = Dehumidifier_Type(HUMIDITY_MIN, HUMIDITY_MAX, dehum_work_time, dehum_day_off_time)

        bookshelf_work_time = ActivityTime_Type(BOOK_START_WORK_HOUR, BOOK_START_WORK_MIN, BOOK_STOP_WORK_HOUR, BOOK_STOP_WORK_MIN)
        bookshelf_day_off_time = ActivityTime_Type(BOOK_START_DAY_OFF_HOUR, BOOK_START_DAY_OFF_MIN, BOOK_STOP_DAY_OFF_HOUR, BOOK_STOP_DAY_OFF_MIN)
        bookshelf_lights = Bookshelf_Type(bookshelf_work_time, bookshelf_day_off_time)

        f.close()
