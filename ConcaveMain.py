# Library imports
import schedule
import time

# User imports
import ConcaveMqtt
import ConcaveProcessing
import ConcaveAutomation
import ConcaveParameters
import ConcaveLogging
import ConcaveSensorData

# Constants
LOG_ID = "MAIN"

# Task periods (seconds)
DATA_PROESSING_PERIOD = 900
DATA_STORAGE_PERIOD = 3600
DEHUMIDIFIER_SCHEDULE_PERIOD = 1800

# Global variables
data_array = []

light_start = None
light_stop = None

client = ConcaveMqtt.mqtt.Client()
client.on_connect = ConcaveMqtt.on_connect
client.on_message = ConcaveMqtt.on_message

# Task functions

# Expiration counters task
def ScheduleMain_TaskManageExpirationCounters():
    ConcaveSensorData.SensorData_Type.Manage_expiration()

# Data processing task
def ScheduleMain_TaskDataProcessing():
    global data_array

    ConcaveProcessing.AddDataLine(data_array)

# Data storing task
def ScheduleMain_TaskStoreCsv():
    global data_array
    
    ConcaveProcessing.StoreLines(data_array)

# Dehumidifier schedule task
def ScheduleMain_TaskDehumidifierSchedule():
    ConcaveAutomation.DehumidifierSchedule()

# Set actuator activity windows taks
def ScheduleMain_TaskSetActivityWindows():
    ConcaveAutomation.SetActivityWindows()

def ScheduleMain_TaskTurnBookshelfOn():
    print("Turn bookshelf lights ON")
    client.publish("zigbee2mqtt/Window_lights/set", "{\"state\":\"ON\"}")

def ScheduleMain_TaskTurnBookshelfOff():
    print("Turn bookshelf lights OFF")
    client.publish("zigbee2mqtt/Window_lights/set", "{\"state\":\"OFF\"}")

def ScheduleMain_TaskTurnDehumOn():
    print("Turn bookshelf lights ON")
    client.publish("zigbee2mqtt/dehumidifier/set", "{\"state\":\"ON\"}")

def ScheduleMain_TaskTurnDehumOff():
    print("Turn bookshelf lights OFF")
    client.publish("zigbee2mqtt/dehumidifier/set", "{\"state\":\"OFF\"}")

# Initalization 
ConcaveLogging.log(LOG_ID, "Concave application started")

# Initialization (Parameters must be the first thing to be called)
ConcaveParameters.Init()
ConcaveSensorData.Init()
ConcaveProcessing.Init()
ConcaveMqtt.Init()
ConcaveAutomation.Init()

# light_start = ConcaveAutomation.GetBookshelfStart()
# light_stop = ConcaveAutomation.GetBookshelfStop()

# light_start = "20:30"
# light_stop = "22:30"

# client.loop_start()

# client.connect("127.0.0.1", 1883, 60)


# Scheduling tasks

schedule.every(1).seconds.do(ScheduleMain_TaskManageExpirationCounters)

schedule.every(DATA_PROESSING_PERIOD).seconds.do(ScheduleMain_TaskDataProcessing)
schedule.every(DATA_STORAGE_PERIOD).seconds.do(ScheduleMain_TaskStoreCsv)

schedule.every(DEHUMIDIFIER_SCHEDULE_PERIOD).seconds.do(ScheduleMain_TaskDehumidifierSchedule)

schedule.every().day.at("00:01").do(ScheduleMain_TaskSetActivityWindows)

schedule.every().day.at(light_start).do(ScheduleMain_TaskTurnBookshelfOn)
schedule.every().day.at(light_stop).do(ScheduleMain_TaskTurnBookshelfOff)


schedule.every().day.at("08:00").do(ScheduleMain_TaskTurnDehumOn)
schedule.every().day.at("08:45").do(ScheduleMain_TaskTurnDehumOff)
schedule.every().day.at("09:15").do(ScheduleMain_TaskTurnDehumOn)
schedule.every().day.at("10:00").do(ScheduleMain_TaskTurnDehumOff)
schedule.every().day.at("11:00").do(ScheduleMain_TaskTurnDehumOn)
schedule.every().day.at("11:30").do(ScheduleMain_TaskTurnDehumOff)
schedule.every().day.at("12:45").do(ScheduleMain_TaskTurnDehumOn)
schedule.every().day.at("13:45").do(ScheduleMain_TaskTurnDehumOff)
schedule.every().day.at("14:15").do(ScheduleMain_TaskTurnDehumOn)
schedule.every().day.at("15:15").do(ScheduleMain_TaskTurnDehumOff)
schedule.every().day.at("15:45").do(ScheduleMain_TaskTurnDehumOn)
schedule.every().day.at("16:30").do(ScheduleMain_TaskTurnDehumOff)
schedule.every().day.at("17:30").do(ScheduleMain_TaskTurnDehumOn)
schedule.every().day.at("18:15").do(ScheduleMain_TaskTurnDehumOff)
schedule.every().day.at("18:45").do(ScheduleMain_TaskTurnDehumOn)
schedule.every().day.at("19:45").do(ScheduleMain_TaskTurnDehumOff)
schedule.every().day.at("20:15").do(ScheduleMain_TaskTurnDehumOn)
schedule.every().day.at("21:00").do(ScheduleMain_TaskTurnDehumOff)
schedule.every().day.at("21:30").do(ScheduleMain_TaskTurnDehumOn)
schedule.every().day.at("22:00").do(ScheduleMain_TaskTurnDehumOff)

while True:
    schedule.run_pending()
    
    time.sleep(1)